Before getting started watch these videos to get familiar with [basics](https://www.youtube.com/watch?v=SWYqp7iY_Tc&feature=youtu.be) of Git and [branching and merging](https://www.youtube.com/watch?v=FyAAIHHClqI&feature=youtu.be) in Git.
**Use of Git is only allowed via terminal. Please [disable](https://stackoverflow.com/questions/17029242/how-do-you-turn-off-version-control-in-android-studio) VCS from your (already bloated) IDEA and in case you are Windows user install [git](https://git-scm.com/downloads). **

## Join as developer
Before executing any commands take a brief look at them all. If you are using git for the first time [add](https://lmgtfy.com/?q=How+to+add+ssh+key+to+gitlab) your SSH key to gitlab.

### Clone & set up
Clone repo use the following command.
```
git clone git@gitlab.com:suruaku/lettu.git
```

Configure your git credentians properly run the following commands.
```
git config --global user.name "<your_name>"
```
```
git config --global user.email "<your_email>"
```

Because Windows and Unix use different EOF(and we do use both of them in this
project), run the following command to autoconvert end of line.
```
git config core.autocrlf true
```

In case you want to join The Elite of this project, run following command as well.
```
git config core.editor vim
```

Quick guide to use Vim. 
Press ```i``` to insert text.
Press ```escape``` ```:wq``` to exit insert mode, write and save current file.

### Working with repo
To make it easier to visualise what is happening run this in your Git bash.
```
echo "alias graph='git log --all --decorate --oneline --graph'" >> ~/.bash_aliases && source ~/.bash_aliases
```
Use command ```graph``` anytime you what to see where is your HEAD and other
branches.

Create a new branch. Give Your branch descriptive name. Usualy a  feature youre
working on. e.g. login, translation-eng. Optin ```-b``` switches to that branch automatically.
```
git checkout -b <branch-name>
```

Make changes and see working directory status.
```
git status
```

Add changed files to index.
```
git add <file>
```
Use ```.``` to add all or ```-u``` to add modifed files.

Commit your changes to the current branch.
Each commit you make is considered a separate unit of change.
```
git commit -m"<your-message>"
```

Push your local branch to remote repository.
There's only one rule: anything in the master branch is always deployable. Remote branch is usually called origin and in our ccase just ```git push``` shoulld be enough.
```
git push <remote-branch> <local-brach>
```
Create MR and wait untill someone reviews your changes. Merge them and delete the feature branch after merging.

Pull new changes. Do this every time before you start working.
```
git pull
```

### About commit messages
Remember these things when you are writing your commint message.

*    Separate subject from body with a blank line
*    Limit the subject line to 50 characters
*    Capitalize the subject line
*    Do not end the subject line with a period
*    Use the imperative mood in the subject line
*    Wrap the body at 72 characters
*    Use the body to explain what and why vs. how

Q: Is commint message some kind of programming language ? 
A: No, but it has its own unwritten rules. Read [this](https://chris.beams.io/posts/git-commit/#seven-rules) article to get more familiar with them.

[See](https://git-scm.com/doc) to learn more about Git.

## Authors 

* **Hugo Hutri** -*Java developer*- 
* **Ilja Kotirinta** -*Java developer*-
