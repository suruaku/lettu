CREATE TABLE account (
    accountid           INTEGER         PRIMARY KEY,
	username            VARCHAR(32)     NOT NULL,
	password            VARCHAR(128)    NOT NULL,
	salt                BINARY(128)     NOT NULL

	                    CHECK (length(username) >= 0)
);

CREATE TABLE university (
	name        		VARCHAR(32)	    PRIMARY KEY
);

CREATE TABLE restaurant (
	restaurantid  		INTEGER		 	PRIMARY KEY,
	universityid 		VARCHAR(32)		NOT NULL,
	name	 			VARCHAR(32)	 	NOT NULL,
	
	FOREIGN KEY (universityid) REFERENCES university (name)
);

CREATE TABLE menu (
	menuid	 		 	INTEGER		 	PRIMARY KEY,
	restaurantid 		INTEGER		 	NOT NULL,
	dateday  			VARCHAR(32)		NOT NULL
);

CREATE TABLE meal (
	mealid	  			INTEGER		 	PRIMARY KEY,
	name 		 		VARCHAR(32) 	NOT NULL,
	category            VARCHAR(32),
    cycle              INTEGER
);

CREATE TABLE rating (
	ratingid	 	 	INTEGER		 	PRIMARY KEY,
	stars 		 		INTEGER		   	NOT NULL,
	review              VARCHAR(100),
	accountid           INTEGER        NOT NULL,
	mealid              INTEGER        NOT NULL,

	CONSTRAINT fk_account
		FOREIGN KEY (accountid) REFERENCES account(accountid)
		ON DELETE CASCADE,
	CONSTRAINT fk_meal
		FOREIGN KEY (mealid) REFERENCES meal(mealid)
		ON DELETE CASCADE
);

CREATE TABLE moderator (
    moderatorid        INTEGER         PRIMARY KEY,
	username		  	VARCHAR(32),
	password            VARCHAR(128)    NOT NULL,
	salt                BINARY(128)     NOT NULL,
	universitid			VARCHAR(32),

	FOREIGN KEY (universitid) REFERENCES university (name)
);

CREATE TABLE meals_menus (
	mealid		  		INTEGER 	NOT NULL,
	menuid		 	 	INTEGER 	NOT NULL,

	CONSTRAINT fk_meal
		FOREIGN KEY (mealid) REFERENCES meal (mealid)
		ON DELETE CASCADE,
	CONSTRAINT fk_menu
		FOREIGN KEY (menuid) REFERENCES menu (menuid)
		ON DELETE CASCADE
);