-- Select all universities
SELECT * FROM university;

-- Select all meals with given mealid
SELECT meal.mealid,name,category,menuid
	FROM meal
	INNER JOIN meals_menus
	ON meal.mealid=meals_menus.mealid
	WHERE menuid=1;
	
-- Select all from account 
SELECT * FROM account WHERE username=?;

-- Search accountid with given username
SELECT accountid FROM account WHERE username=?;

-- Change username
UPDATE account
	SET username=?
	WHERE accountid=?;

-- Create new account
INSERT INTO account(username, password, hash)
	VALUES(?, ?, ?);

-- Select moderatorid with given username
SELECT moderatorid FROM moderator WHERE username=?;

-- Select all from moderator
SELECT * FROM moderator WHERE username=?;

-- Create new moderator
INSERT INTO moderator(username, password, hash)
	VALUES(?, ?, ?);

-- Select all ratings from user
SELECT * FROM rating WHERE accountid=?;

-- Select every restaurant
SELECT * FROM restaurant;

-- Select all menus in restaurant
SELECT * FROM menu WHERE restaurantid=?;

-- Select all repeating meals
SELECT * FROM meal WHERE cycle IS NOT NULL;

-- Enable cascade
PRAGMA foreign_keys = ON

-- Delete rows with given id 

DELETE meal WHERE mealid=?;
DELETE restaurant WHERE restaurantid=?;
DELETE account WHERE accountid=?;

-- Drop all tables

DROP TABLE IF EXISTS meals_menus;
DROP TABLE IF EXISTS meal;
DROP TABLE IF EXISTS menu;
DROP TABLE IF EXISTS restaurant;
DROP TABLE IF EXISTS university;
DROP TABLE IF EXISTS moderator;
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS rating;

-- Insert some base values

INSERT INTO university
(name)
VALUES
("LUT");

INSERT INTO restaurant
(restaurantid, universityid, name)
VALUES
(1, "LUT", "Laseri");

INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(1, "2019-07-23", 1);

INSERT INTO meal
(mealid, name, category, cycle)
VALUES
(1, "Lohikeitto", "Kokin suositus", 2);

INSERT INTO meal
(mealid, name, category)
VALUES
(2, "Pizza", "Kasvisherkkuja");

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(1, 1);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(1, 2);

INSERT INTO moderator
(username, password, salt)
VALUES
("admin", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');

INSERT INTO account
(username, password, salt)
VALUES
("jappe", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(1, 4, "Ihan maukasta ruokaa", 1, 1);

