CREATE TABLE account (
    accountid           INTEGER         PRIMARY KEY,
	username            VARCHAR(32)     NOT NULL,
	password            VARCHAR(128)    NOT NULL,
	salt                BINARY(128)     NOT NULL

	                    CHECK (length(username) >= 0)
);

CREATE TABLE university (
	name        		VARCHAR(32)	    PRIMARY KEY
);

CREATE TABLE restaurant (
	restaurantid  		INTEGER		 	PRIMARY KEY,
	universityid 		VARCHAR(32)		NOT NULL,
	name	 			VARCHAR(32)	 	NOT NULL,
	
	FOREIGN KEY (universityid) REFERENCES university (name)
);

CREATE TABLE menu (
	menuid	 		 	INTEGER		 	PRIMARY KEY,
	restaurantid 		INTEGER		 	NOT NULL,
	dateday  			VARCHAR(32)		NOT NULL
);

CREATE TABLE meal (
	mealid	  			INTEGER		 	PRIMARY KEY,
	name 		 		VARCHAR(32) 	NOT NULL,
	category            VARCHAR(32),
    cycle              INTEGER
);

CREATE TABLE rating (
	ratingid	 	 	INTEGER		 	PRIMARY KEY,
	stars 		 		INTEGER		   	NOT NULL,
	review              VARCHAR(100),
	accountid           INTEGER        NOT NULL,
	mealid              INTEGER        NOT NULL,

	CONSTRAINT fk_account
		FOREIGN KEY (accountid) REFERENCES account(accountid)
		ON DELETE CASCADE,
	CONSTRAINT fk_meal
		FOREIGN KEY (mealid) REFERENCES meal(mealid)
		ON DELETE CASCADE
);

CREATE TABLE moderator (
    moderatorid        INTEGER         PRIMARY KEY,
	username		  	VARCHAR(32),
	password            VARCHAR(128)    NOT NULL,
	salt                BINARY(128)     NOT NULL,
	universitid			VARCHAR(32),

	FOREIGN KEY (universitid) REFERENCES university (name)
);

CREATE TABLE meals_menus (
	mealid		  		INTEGER 	NOT NULL,
	menuid		 	 	INTEGER 	NOT NULL,

	CONSTRAINT fk_meal
		FOREIGN KEY (mealid) REFERENCES meal (mealid)
		ON DELETE CASCADE,
	CONSTRAINT fk_menu
		FOREIGN KEY (menuid) REFERENCES menu (menuid)
		ON DELETE CASCADE
);


INSERT INTO university
(name)
VALUES
("LUT");


INSERT INTO restaurant
(restaurantid, universityid, name)
VALUES
(1, "LUT", "Laseri");

INSERT INTO restaurant
(restaurantid, universityid, name)
VALUES
(2, "LUT", "Buffet");

INSERT INTO restaurant
(restaurantid, universityid, name)
VALUES
(3, "LUT", "Yolo");



INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(1, "2019-07-23", 1);

INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(2, "2019-07-23", 2);

INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(3, "2019-07-23", 3);


INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(1, "2019-07-24", 4);

INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(2, "2019-07-24", 5);

INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(3, "2019-07-24", 6);

INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(1, "2019-07-25", 7);

INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(2, "2019-07-25", 8);

INSERT INTO menu
(restaurantid, dateday, menuid)
VALUES
(3, "2019-07-25", 9);



INSERT INTO meal
(mealid, name, category, cycle)
VALUES
(1, "Lohikeitto", "Kokin suositus", 2);

INSERT INTO meal
(mealid, name, category)
VALUES
(2, "Pizza", "Kasvisherkkuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(3, "Kiinalaista vettä kristallilasissa, koivunlehtisellä kastikkeella", "Kotoisia makuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(4, "Lohi ja peruna", "Kokin suositus");

INSERT INTO meal
(mealid, name, category)
VALUES
(5, "Nauhdan pihvi", "Kasvisherkkuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(6, "Kukko", "Kasvisherkkuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(7, "Pataa", "Kokin suositus");

INSERT INTO meal
(mealid, name, category)
VALUES
(8, "Pihvi", "Kotoisia makuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(9, "Vesi", "Kasvisherkkuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(10, "Pinaattipataa", "Kokin suositus");

INSERT INTO meal
(mealid, name, category)
VALUES
(11, "Lehtipihvi", "Kotoisia makuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(12, "Vesitortilla", "Kasvisherkkuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(13, "Makkarakeitto", "Kokin suositus");

INSERT INTO meal
(mealid, name, category)
VALUES
(14, "Rullakebab nugeteilla", "Kasvisherkkuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(15, "Kiinalaista vettä kastikkeella", "Kotoisia makuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(16, "Lohi ja silakka", "Kokin suositus");

INSERT INTO meal
(mealid, name, category)
VALUES
(17, "Nauhdan maksaa", "Kasvisherkkuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(18, "Kasvi", "Kasvisherkkuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(19, "Kaurapuuro", "Kokin suositus");

INSERT INTO meal
(mealid, name, category)
VALUES
(20, "Porsas", "Kotoisia makuja");

INSERT INTO meal
(mealid, name, category)
VALUES
(21, "Kaktus", "Kasvisherkkuja");



INSERT INTO meals_menus
(menuid, mealid)
VALUES
(1, 1);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(1, 2);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(1, 3);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(2, 19);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(2, 20);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(2, 21);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(3, 4);


INSERT INTO meals_menus
(menuid, mealid)
VALUES
(3, 5);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(3, 6);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(4, 7);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(4, 8);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(4, 9);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(5, 10);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(5, 11);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(5, 12);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(6, 13);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(6, 14);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(6, 15);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(7, 16);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(7, 17);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(7, 18);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(8, 19);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(8, 20);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(8, 21);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(9, 7);

INSERT INTO meals_menus
(menuid, mealid)
VALUES
(9, 8);


INSERT INTO moderator
(username, password, salt)
VALUES
("admin", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');


INSERT INTO account
(username, password, salt)
VALUES
("jappe", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');

INSERT INTO account
(username, password, salt)
VALUES
("pasi", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');

INSERT INTO account
(username, password, salt)
VALUES
("pekka", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');

INSERT INTO account
(username, password, salt)
VALUES
("juhani", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');

INSERT INTO account
(username, password, salt)
VALUES
("robert", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');

INSERT INTO account
(username, password, salt)
VALUES
("olli", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');

INSERT INTO account
(username, password, salt)
VALUES
("maria", "381c72ca234d05161d6f3a108b7ae5e0df126f6d70c9a4184fdc51a08fe40e54d1c6ce465c9ede2ad54bf321635b3d200dca1e5e40d3431921d34ad60d7eb95a", X'E1253D9A61C4FF664F5E0112038F1434');



INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(1, 4, "Ihan maukasta ruokaa", 1, 1);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(2, 3, "Kaverit piti tosi paljon", 2, 2);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(3, 4, "Jotenkin tuntui että tää oli aika mautonta. Ihan syötävää kuitenkin, kunhan muistaa laittaa suolaa ja pippuria!", 3, 3);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(4, 3, "Keskiverto", 4, 4);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(5, 1, "hyi!!", 5, 5);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(6, 2, "Liian tulinen", 6, 6);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(7, 4, "Hyvvee", 7, 7);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(8, 3, "Iha jees", 1, 2);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(9, 2, "Ei maistunut", 2, 3);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(10, 2, "Ei oo kivaa", 4, 10);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(11, 1, "Liian kuivaa", 4, 12);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(12, 5, "Loistavaaa", 5, 7);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(13, 1, "liian huono", 5, 9);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(14, 1, "Tosi mauton", 3, 9);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(15, 5, "parasta nyt", 2, 5);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(16, 4, "ihan jees", 4, 3);

INSERT INTO rating
(ratingid, stars, review, accountid, mealid)
VALUES
(17, 2, "ei kyl toimi", 7, 7);