package com.example.lettu.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lettu.R;
import com.example.lettu.data.Account;
import com.example.lettu.database.DatabaseManager;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

import static androidx.appcompat.app.AppCompatDelegate.setDefaultNightMode;

public class AccountActivity extends AppCompatActivity {

    private static final String TAG = "AccountActivity";

    ArrayList<Account> accounts;
    int veryImportant = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        Log.d(TAG, "onCreate: started.");

        Button button_logout    = (Button) findViewById(R.id.button_logout);
        Button button_confirm   = (Button) findViewById(R.id.button_confirm);
        Button button_admin     = (Button) findViewById(R.id.button_admin);
        ImageView imageView4    = (ImageView) findViewById(R.id.imageView4);
        final EditText username = (EditText) findViewById(R.id.username);

        // db instance
        final DatabaseManager databasemanager = DatabaseManager.getInstance();
        accounts = databasemanager.getAccountsFromDatabase();

        // Set username and adminpanel button
        if (MainActivity.getModerator() == null) {
            username.setText(MainActivity.getAccount().getUsername());
            button_admin.setVisibility(View.GONE);

        } else {
            username.setText(MainActivity.getModerator().getUsername());
            button_confirm.setVisibility(View.GONE);
            username.setFocusable(false);
            username.setFocusableInTouchMode(false);
        }


        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newUsername;
                Context context = getApplicationContext();
                boolean registration_valid = true;
                newUsername = username.getText().toString();
                /*
                for (Account account : accounts) {
                    if (account.getUsername().equals(newUsername)) {
                        Toast toast = Toast.makeText(context, "Username is already taken!", duration);
                        toast.show();
                        return;
                    }
                }
                */
                if(DatabaseManager.getInstance().getModeratorId(newUsername) != null){
                    Toasty.warning(context, getString(R.string.register_same_username), Toast.LENGTH_SHORT, true).show();
                    return;
                }
                if(DatabaseManager.getInstance().getAccountId(newUsername) == null) {
                    Account account = MainActivity.getAccount();
                    account.setUsername(newUsername);
                    MainActivity.setAccount(account);
                    // OK
                    databasemanager.setAccount(account.getId(), account.getUsername(), account.getPassword(), account.getSalt());
                    //notify user
                    Toasty.success(context, R.string.change_username, Toast.LENGTH_SHORT, true).show();
                }
                else {
                    Toasty.warning(context, getString(R.string.register_same_username), Toast.LENGTH_SHORT, true).show();
                }

            }
        });

        // Very important method!!
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                veryImportant++;
                if (veryImportant >= 8) {
                    Toasty.normal(getApplicationContext(), "Night mode activated!", getResources().getDrawable(R.drawable.darkmode512)).show();
                    setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            }
        });

        // log out
        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountActivity.this, MainActivity.class);
                MainActivity.setAccount(null);
                MainActivity.setModerator(null);
                startActivity(intent);
            }
        });

        // change to admin panel
        button_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountActivity.this, AdminActivity.class);
                MainActivity.setAccount(null);
                startActivity(intent);
            }
        });
    }
}