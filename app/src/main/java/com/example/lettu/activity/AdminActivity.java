package com.example.lettu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.database.DatabaseUtilsCompat;

import com.example.lettu.R;
import com.example.lettu.data.Account;
import com.example.lettu.data.Meal;
import com.example.lettu.data.Menu;
import com.example.lettu.data.Rating;
import com.example.lettu.data.Restaurant;
import com.example.lettu.data.University;
import com.example.lettu.data.writeJSON;
import com.example.lettu.database.DatabaseHelper;
import com.example.lettu.database.DatabaseManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;


public class AdminActivity extends AppCompatActivity {

    private static final String TAG = "AdminActivity";

    static ArrayList<Account> accounts;
    static ArrayList<Meal> meals;
    static ArrayList<Menu> menus;
    static ArrayList<Restaurant> restaurants;

    Spinner     admin_spinner_restaurants;
    Spinner     admin_spinner_meals;
    Spinner     admin_spinner_accounts;

    EditText    admin_editText_restaurant;
    EditText    admin_editText_meal;
    EditText    admin_editText_date;
    EditText    admin_editText_category;
    EditText    days;

    Button      admin_restaurant_add;
    Button      admin_restaurant_remove;
    Button      admin_restaurant_edit;
    Button      admin_restaurant_froze;

    Button      admin_meal_add;
    Button      admin_meal_remove;

    Button      admin_account_remove;
    Button      admin_account_view;

	Button      admin_button_json;


    DatabaseManager databasemanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

	    setContentView(R.layout.activity_admin);
        Log.d(TAG, "onCreate: started.");

        // Views
        admin_spinner_restaurants   = findViewById(R.id.admin_spinner_restaurants);
        admin_spinner_meals         = findViewById(R.id.admin_spinner_meals);
        admin_spinner_accounts      = findViewById(R.id.admin_spinner_accounts);

        admin_editText_restaurant   = findViewById(R.id.admin_editText_restaurant);
        admin_editText_meal         = findViewById(R.id.admin_editText_meal);
        admin_editText_category     = findViewById(R.id.admin_editText_category);
        admin_editText_date         = findViewById(R.id.admin_editText_date);

        admin_restaurant_add        = findViewById(R.id.admin_restaurant_add);
        admin_restaurant_remove     = findViewById(R.id.admin_restaurant_remove);
        admin_restaurant_edit       = findViewById(R.id.admin_restaurant_edit);
        admin_restaurant_froze      = findViewById(R.id.admin_restaurant_froze);

        admin_meal_add              = findViewById(R.id.admin_meal_add);
        admin_meal_remove           = findViewById(R.id.admin_meal_remove);

        admin_account_remove        = findViewById(R.id.admin_account_remove);
        admin_account_view          = findViewById(R.id.admin_account_view);

	    admin_button_json           = findViewById(R.id.admin_button_json);

	    days                        = findViewById(R.id.days);


	    // init db
        databasemanager   = DatabaseManager.getInstance();
        // init arrays
        accounts          = databasemanager.getAccountsFromDatabase();
        meals             = databasemanager.getMealsFromDatabase();
        restaurants       = databasemanager.getRestaurantsFromDatabase();

        setAdapterForAccounts();
        setAdapterForMeals();
        setAdapterForRestaurants();

	    writeJSON writeJSON = new writeJSON();

	    // write json to file and notify user
	    admin_button_json.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    File file = new File(getApplicationContext().getExternalCacheDir(),"lettuJSON.json");
			    try {
				    FileOutputStream out = new FileOutputStream(file);
				    writeJSON.writeJsonStream(out);
				    Log.d(TAG, file.getAbsolutePath());
				    Toasty.success(getApplicationContext(), getString(R.string.admin_json_notify_success), Toast.LENGTH_SHORT, true).show();
			    } catch (IOException e) {
				    e.printStackTrace();
				    Toasty.error(getApplicationContext(), getString(R.string.admin_json_notify_error), Toast.LENGTH_SHORT, true).show();
			    }
		    }
	    });
    }

    // View all ratings made by user
    public void viewRatings(View v){
        Account account = (Account) admin_spinner_accounts.getSelectedItem();
        AdminRatingActivity.setAccount(account);
        Intent intent = new Intent(AdminActivity.this, AdminRatingActivity.class);
        startActivity(intent);
    }


    // Add a meal to the database and to mainActivity
    public void addMeal(View v){
        String meal_name = admin_editText_meal.getText().toString();
        String meal_category = admin_editText_category.getText().toString();
        String menu_date = admin_editText_date.getText().toString();
        Integer menuid = null;
        int restaurantid = ((Restaurant)admin_spinner_restaurants.getSelectedItem()).getId();

        // Look for existing menu
        menus = databasemanager.getMenusFromDatabaseWithValue(Integer.toString(restaurantid), DatabaseHelper.COLUMN_MENU_RESTAURANT_KEY);
        for(Menu menu : menus) {
            if(menu.getDate().equals(menu_date)){
                menuid = menu.getId();
                break;
            }
        }

        //
        int restaurant_position = admin_spinner_restaurants.getSelectedItemPosition();
        ArrayList<University> universities = MainActivity.getUniversities();
        ArrayList<Menu> menu_list = universities.get(0).getRestaurants().get(restaurant_position).getMenus();

        // Create a new menu if necessary
        if(menuid == null) {
            menuid = databasemanager.setMenu(null, restaurantid, menu_date);
            menu_list.add(new Menu(menu_date, menuid, restaurantid));
        }
        Integer mealid = databasemanager.setMeal(null, menuid, meal_name, meal_category);
        Meal meal = new Meal(meal_name, meal_category, mealid);
        for(Menu menu : menu_list){
            if(menu.getId() == menuid){
                menu.addMeal(meal);
            }
        }
        meals.add(meal);
        restaurants.get(restaurant_position).setMenus(menu_list);
        universities.get(0).setRestaurants(restaurants);
        MainActivity.setUniversities(universities);

        Toasty.info(getApplicationContext(), "Meal added!", Toast.LENGTH_LONG, true).show();
    }

    public void frozeRestaurant(View v){
        int frozed_days = Integer.parseInt(days.getText().toString());
        int restaurantid = ((Restaurant)admin_spinner_restaurants.getSelectedItem()).getId();
        ArrayList<Integer> menuids = new ArrayList<>();

        ArrayList<University> universities = MainActivity.getUniversities();
        restaurants = universities.get(0).getRestaurants();
        for(Restaurant restaurant : restaurants){
            if(restaurant.getId() == restaurantid){
                ArrayList<Menu> menus = restaurant.getMenus();
                for(int i = 0; i<menus.size(); i++) {
                    Menu menu = menus.get(i);
                    SimpleDateFormat ISOFormatter = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date date_menu =  ISOFormatter.parse(menu.getDate());
                        Date date_min = ISOFormatter.parse(MainActivity.getISODate());
                        long time_between = date_menu.getTime() - date_min.getTime();
                        long days = TimeUnit.DAYS.convert(time_between, TimeUnit.MILLISECONDS);
                        if((days >= 0)&&(days <= (long)frozed_days)) {
                            // Remove the menu, because it is in frozen time period
                            menuids.add(menu.getId());
                            menus.remove(i);
                            continue;
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
                restaurant.setMenus(menus);
            }
        }
        for(Integer id : menuids) {
            databasemanager.removeMenu(id);
        }
        universities.get(0).setRestaurants(restaurants);
        MainActivity.setUniversities(universities);
        databasemanager.removeRestaurant(restaurantid);
        Log.d(TAG, "Restaurant frozen");
        Toasty.info(getApplicationContext(), "Restaurant frozen!", Toast.LENGTH_LONG, true).show();
    }

    public void addRestaurant(View v){
        String restaurant_name = admin_editText_restaurant.getText().toString();
        Integer restaurantid = databasemanager.setRestaurant(null, "LUT", restaurant_name);
        Restaurant restaurant = new Restaurant(restaurant_name, restaurantid);
        ArrayList<University> universities = MainActivity.getUniversities();
        universities.get(0).addRestaurant(restaurant);
        MainActivity.setUniversities(universities);

        Toasty.info(getApplicationContext(), "Restaurant added!", Toast.LENGTH_LONG, true).show();
    }

    public void removeAllMealsWithNameFromRestaurant(View v){
        int meal_position       = admin_spinner_meals.getSelectedItemPosition();
        int restaurant_position = admin_spinner_restaurants.getSelectedItemPosition();
        //ArrayList<Menu> menus   = restaurants.get(restaurant_position).getMenus();


        // Select all meals with same name from the restaurant
        //ArrayList<Integer> mealid_list = databasemanager.getMealIdfromDatabaseWithValues(meal_name, Integer.toString(restaurantid));

        // Remove meal from database with given id
        int mealid = meals.get(meal_position).getId();
        databasemanager.removeMeal(mealid);

        meals.remove(meal_position);
        ArrayList<University> universities = MainActivity.getUniversities();
        ArrayList<Restaurant> restaurants = universities.get(0).getRestaurants();
        menus = restaurants.get(restaurant_position).getMenus();
        ArrayList<Meal> meal_list;
        loop:
        for(Menu menu : menus){
            meal_list = menu.getMeals();
            for(int i = 0; i < meal_list.size(); i++){
                if(meal_list.get(i).getId() == mealid){
                    meal_list.remove(i);
                    menu.setMeals(meal_list);
                    break loop;
                }
            }
        }
        universities.get(0).getRestaurants().get(restaurant_position).setMenus(menus);
        MainActivity.setUniversities(universities);

        Toasty.info(getApplicationContext(), "Meal removed!", Toast.LENGTH_LONG, true).show();
    }

    public void removeRestaurant(View v) {
        int restaurantid = ((Restaurant)admin_spinner_restaurants.getSelectedItem()).getId();

        ArrayList<University> universities = MainActivity.getUniversities();
        ArrayList<Restaurant> restaurant_list = universities.get(0).getRestaurants();
        for(int i = 0; i < restaurant_list.size(); i++){
            Restaurant restaurant = restaurant_list.get(i);
            if(restaurant.getId() == restaurantid){
                restaurant_list.remove(i);
                break;
            }
        }
        universities.get(0).setRestaurants(restaurant_list);
        MainActivity.setUniversities(universities);
        databasemanager.removeRestaurant(restaurantid);
        Log.d(TAG, "Restaurant removed from the database");
        Toasty.info(getApplicationContext(), "Restaurant removed!", Toast.LENGTH_LONG, true).show();
    }

    public void removeAccount(View v){
        int accountid = ((Account)admin_spinner_accounts.getSelectedItem()).getId();
        databasemanager.removeAccount(accountid);
        ArrayList<Rating> ratings = MainActivity.getRatings();
        for(int i = 0; i<ratings.size(); i++) {
            if(ratings.get(i).getAccountid() == accountid){
                ratings.remove(i);
            } else{
                i++;
            }
        }
        MainActivity.setRatings(ratings);
        Toasty.info(getApplicationContext(), "Account removed!", Toast.LENGTH_LONG, true).show();
    }

    public void editRestaurant(View v){
        String restaurant_name = admin_editText_restaurant.getText().toString();
        int restaurantid = ((Restaurant)admin_spinner_restaurants.getSelectedItem()).getId();

        ArrayList<University> universities = MainActivity.getUniversities();
        ArrayList<Restaurant> restaurants = universities.get(0).getRestaurants();
        for(Restaurant restaurant : restaurants){
            if(restaurant.getId() == restaurantid){
                restaurant.setName(restaurant_name);
            }
        }
        universities.get(0).setRestaurants(restaurants);
        MainActivity.setUniversities(universities);

        databasemanager.setRestaurant(restaurantid, "LUT", restaurant_name);

        Toasty.info(getApplicationContext(), "Restaurant edited!", Toast.LENGTH_LONG, true).show();
    }

    private void setAdapterForMeals(){
        // Set adapter for restaurants
        ArrayAdapter<Restaurant> restaurant_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, restaurants);
        restaurant_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        admin_spinner_restaurants.setAdapter(restaurant_adapter);
    }

    private void setAdapterForRestaurants(){
        // Set adapter for meals
        ArrayAdapter<Meal> meal_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, meals);
        meal_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        admin_spinner_meals.setAdapter(meal_adapter);
    }

    private void setAdapterForAccounts(){
        // Set adapter for accounts
        ArrayAdapter<Account> account_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, accounts);
        account_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        admin_spinner_accounts.setAdapter(account_adapter);
    }

    @Override
    public void onBackPressed() {
        MainActivity.notifyAdapter();
        super.onBackPressed();
    }
}

