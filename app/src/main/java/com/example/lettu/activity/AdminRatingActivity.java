
package com.example.lettu.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lettu.R;
import com.example.lettu.adapter.AdminRatingListAdapter;
import com.example.lettu.data.Account;
import com.example.lettu.data.Meal;
import com.example.lettu.data.Rating;
import com.example.lettu.database.DatabaseHelper;
import com.example.lettu.database.DatabaseManager;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdminRatingActivity extends AppCompatActivity {

    private static final String TAG = "AdminRatingActivity";
    private static Account account;

    AdminRatingListAdapter adapter;
    RecyclerView user_rating_list;
    ArrayList<Meal> meals;
    Rating editable_rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_rating);
        Log.d(TAG, "onCreate: started.");
        user_rating_list = findViewById(R.id.admin_review_list);
        TextView user_meal = findViewById(R.id.user_meal);
        TextView user_name = findViewById(R.id.textView2);
        user_meal.setText("Click rating to delete");
        user_name.setText(account.getUsername());

        ArrayList<Rating> ratings = DatabaseManager.getInstance().getRatingsFromDatabaseWithValue(Integer.toString(account.getId()), DatabaseHelper.COLUMN_RATING_ACCOUNT_KEY);
        account.setRatings(ratings);

        meals = DatabaseManager.getInstance().getMealsFromDatabase();
        adapter = new AdminRatingListAdapter(ratings, meals, this);
        user_rating_list.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(AdminRatingActivity.this);
        user_rating_list.setLayoutManager(layoutManager);
    }

    public static void setAccount(Account account) {
        AdminRatingActivity.account = account;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
