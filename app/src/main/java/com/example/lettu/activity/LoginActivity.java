package com.example.lettu.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.lettu.data.Account;
import com.example.lettu.data.Moderator;
import com.example.lettu.data.writeJSON;
import com.example.lettu.database.DatabaseHelper;
import com.example.lettu.database.DatabaseManager;
import com.example.lettu.R;


import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

import static com.example.lettu.activity.MainActivity.account;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    ArrayList<Account> accounts;
    Toast toast;
    TextView randomNumber;
    EditText authTextView;
    ImageButton verificationButton;
    Account account;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Log.d(TAG, "onCreate: started.");

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final Button registerButton = findViewById(R.id.register);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);
        randomNumber = findViewById(R.id.login_randon_numbers);
        authTextView = findViewById(R.id.login_auth);
        verificationButton = findViewById(R.id.verificationButton);

        authTextView.setVisibility(View.GONE);
        verificationButton.setVisibility(View.GONE);

        toast = Toast.makeText(getApplicationContext(), "!", Toast.LENGTH_SHORT);
        toast.setGravity(0,0,200);

        final DatabaseManager databasemanager = DatabaseManager.getInstance();
        accounts = databasemanager.getAccountsFromDatabase();


        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                }
                return false;
            }
        });

        // Check if username and password is correct and if so, log in
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                usernameEditText.setError(null);
                passwordEditText.setError(null);
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                Moderator moderator;
                Integer accountid = DatabaseManager.getInstance().getAccountId(username);
                Integer moderatorid = DatabaseManager.getInstance().getModeratorId(username);
                if(accountid != null) {
                    account = DatabaseManager.getInstance().getAccountsFromDatabaseWithValue(Integer.toString(accountid), DatabaseHelper.COLUMN_ACCOUNT_PRIMARY_KEY).get(0);
                    byte[] salt = account.getSalt();
                    String hashed_password = get_SHA_512_SecurePassword(password, salt);
                    if (account.getUsername().equals(username) && account.getPassword().equals(hashed_password)) {
                        startVerification();
                        loadingProgressBar.setVisibility(View.INVISIBLE);
                        return;
                    }
                    return;
                // If user is moderator log in as moderator and notify with really cool toast
                } else if(moderatorid != null) {
                    moderator = DatabaseManager.getInstance().getModeratorsFromDatabaseWithValue(Integer.toString(moderatorid), DatabaseHelper.COLUMN_MODERATOR_PRIMARY_KEY).get(0);
                    byte[] salt = moderator.getSalt();
                    String hashed_password = get_SHA_512_SecurePassword(password, salt);
                    if (moderator.getUsername().equals(username) && moderator.getPassword().equals(hashed_password)) {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        MainActivity.setModerator(moderator);
                        // custom toast
                        Toasty.Config.getInstance()
                                .setToastTypeface(Typeface.createFromAsset(getAssets(), "PCap Terminal.otf"))
                                .allowQueue(false)
                                .apply();
                        Toasty.custom(getApplicationContext(), R.string.sudo_toast, getResources().getDrawable(R.drawable.laptop512),
                                android.R.color.black, android.R.color.holo_green_light, Toasty.LENGTH_LONG, true, true).show();
	                    Toasty.Config.reset();
                        startActivity(intent);
                    }
                    return;
                }
                passwordEditText.setError(getString(R.string.login_wrong));
                loadingProgressBar.setVisibility(View.INVISIBLE);
            }
        });

        // Register a new account
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] salt;
                boolean registration_valid = true;
                loadingProgressBar.setVisibility(View.VISIBLE);
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                // check password
                if(!isPasswordStrong(password)) {
                    passwordEditText.setError(getString(R.string.invalid_password));
                    loadingProgressBar.setVisibility(View.INVISIBLE);
                    registration_valid = false;
                }
                //check if username exists
                if(DatabaseManager.getInstance().getAccountId(username) != null) {
                    usernameEditText.setError(getString(R.string.register_same_username));
                    registration_valid = false;
                }
                //check if moderatorname exists
                if(DatabaseManager.getInstance().getModeratorId(username) != null) {
                    usernameEditText.setError(getString(R.string.register_same_username));
                    registration_valid = false;
                }


                if(registration_valid) {
                    try {
                        salt = getSalt();
                        String hashed_password = get_SHA_512_SecurePassword(password, salt);
                        databasemanager.setAccount(null, username, hashed_password, salt);
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    Toasty.success(getApplicationContext(), getString(R.string.registration_successful), Toast.LENGTH_SHORT, true).show();
                    accounts = databasemanager.getAccountsFromDatabase();
                }
                loadingProgressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    // Make verification text visible
    private void startVerification(){
        generateRandomNumberScreen();
        authTextView.setVisibility(View.VISIBLE);
        verificationButton.setVisibility(View.VISIBLE);

    }

    public void testVerification(View v){
        if(randomNumber.getText().toString().equals(authTextView.getText().toString())){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            MainActivity.setAccount(account);
            startActivity(intent);
        } else {
            generateRandomNumberScreen();
        }
    }

    // Checks if password is strong
    private boolean isPasswordStrong(String password) {
        Pattern pattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(password);
        if(password.length() > 12) {
            // If password contains a special character
            if (matcher.find()) {
                // If password contains a number
                if (password.matches(".*\\d.*")) {
                    if (password.toUpperCase() != password && password.toLowerCase() != password) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // SHA512
    private static String get_SHA_512_SecurePassword(String passwordToHash, byte[] salt)
    {
        String generatedPassword = null;
        try {
            // use SHA512
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            // Magic
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }


    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    //Get salt for hashing
    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        //Log.d(TAG, bytesToHex(salt));
        return salt;
    }

    private void generateRandomNumberScreen(){
        String chars = "0123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        while (sb.length() < 6) { // length of the random string.
            int index = (int) (random.nextFloat() * chars.length());
            sb.append(chars.charAt(index));
        }
        randomNumber.setText(sb.toString());
    }
}
