package com.example.lettu.activity;

import android.content.Intent;
import android.os.Bundle;

import com.example.lettu.data.Account;
import com.example.lettu.data.Meal;
import com.example.lettu.R;
import com.example.lettu.data.Moderator;
import com.example.lettu.data.Rating;
import com.example.lettu.data.Restaurant;
import com.example.lettu.data.University;
import com.example.lettu.adapter.MenuListAdapter;
import com.example.lettu.database.DatabaseHelper;
import com.example.lettu.database.DatabaseManager;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import es.dmoral.toasty.Toasty;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";
    private static Calendar calendar;

    DatabaseManager databasemanager;
    static ArrayList<Rating> ratings;
    static ArrayList<Meal> meals;
    static ArrayList<com.example.lettu.data.Menu> menus;
    static ArrayList<Restaurant> restaurants;
    static ArrayList<University> universities;
    static Account account;
    static Moderator moderator;
    static MenuListAdapter adapter;
    SimpleDateFormat formatter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To change theme just put your theme id.
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: started.");

        DatabaseManager.initializeInstance(new DatabaseHelper(getApplicationContext()));
        // Find views
        Toolbar toolbar                 = findViewById(R.id.toolbar);
        DrawerLayout drawer             = findViewById(R.id.drawer_layout);
        NavigationView navigationView   = findViewById(R.id.nav_view);
        ImageButton button_prev         = findViewById(R.id.button_prev);
        ImageButton button_next         = findViewById(R.id.button_next);
        recyclerView                    = findViewById(R.id.recycler_view);
        final TextView datebox          = findViewById(R.id.datebox);

        // Create database and table
        databasemanager = DatabaseManager.getInstance();
        loadObjectsFromDatabase();

        initRecyclerView();

        //Get date instance and make
        calendar = Calendar.getInstance();
        formatter = new SimpleDateFormat("dd-MM-yyyy");
        calendar.set(2019, 06, 24);
        datebox.setText(formatter.format(calendar.getTime()));

        Log.d(TAG, universities.get(0).getRestaurants().get(0).toString());

        restaurants = universities.get(0).getRestaurants();

	    updateRecyclerView();
	    recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Create toolbar
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null && extras.containsKey("account")) {
            account = (Account) getIntent().getSerializableExtra("account");
        }

        button_prev.setOnClickListener(view -> {
            calendar.add(Calendar.DATE, -1);
            if( false) {
                Toasty.warning(getApplicationContext(), R.string.no_prevmenu, Toast.LENGTH_SHORT, true).show();
            } else  {
                // Change date
                datebox.setText(formatter.format(calendar.getTime()));
                //updateMenus();
                updateRecyclerView();
            }
        });

        button_next.setOnClickListener(view -> {
            if( false ) {
                Toasty.warning(getApplicationContext(), R.string.no_nextmenu, Toast.LENGTH_SHORT, true).show();
            } else {
                // Change date
                calendar.add(Calendar.DATE, 1);
                datebox.setText(formatter.format(calendar.getTime()));
                //updateMenus();
                //databasemanager.updateCyclingMenusInDatabase();
                updateRecyclerView();
            }
        });
    }

    private void checkForRepeatingMeals(){
    }

    private void updateRecyclerView() {
        adapter = new MenuListAdapter(restaurants, this);
        recyclerView.setAdapter(adapter);
    }


    public static Account getAccount() {
        return account;
    }


    // Return string on ISO-format
    public static String getISODate(){
        SimpleDateFormat ISOFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = ISOFormatter.format(calendar.getTime());
        return date;
    }


    public static void setAccount(Account account) { MainActivity.account = account; }


    private void loadObjectsFromDatabase(){
        databasemanager.getInstance();
        universities = databasemanager.getUniversitiesFromDatabase();
        for(University university : universities) {
            String universityid = university.getName();
            ArrayList<Restaurant> restaurant_list = databasemanager.getRestaurantsFromDatabaseWithValue(universityid, DatabaseHelper.COLUMN_RESTAURANT_UNIVERSITY_KEY);

            for(Restaurant restaurant : restaurant_list) {
                String restaurantid = Integer.toString(restaurant.getId());
                ArrayList<com.example.lettu.data.Menu> menu_list = databasemanager.getMenusFromDatabaseWithValue(restaurantid, DatabaseHelper.COLUMN_MENU_RESTAURANT_KEY);

                for(com.example.lettu.data.Menu menu : menu_list) {
                    String menuid = Integer.toString(menu.getId());
                    ArrayList<Meal> meal_list = databasemanager.getMealsFromDatabaseWithValue(menuid, DatabaseHelper.COLUMN_MEALS_MENUS_MENU_KEY);

                    for(Meal meal : meal_list) {
                        String mealid = Integer.toString(meal.getId());
                        ArrayList<Rating> rating_list = databasemanager.getRatingsFromDatabaseWithValue(mealid, DatabaseHelper.COLUMN_RATING_MEAL_KEY);
                        meal.setRatings(rating_list);
                        meal.calculateAverageRating();
                    }
                    menu.setMeals(meal_list);
                }
                restaurant.setMenus(menu_list);
            }
            university.setRestaurants(restaurant_list);
        }
    }


    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: init");
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    public static void notifyAdapter(){
        adapter.notifyDataSetChanged();
    }


    public static void setModerator(Moderator moderator) {
        MainActivity.moderator = moderator;
    }


    public static Moderator getModerator() {
        return moderator;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Main view
        } else if (id == R.id.nav_account) {
            if (MainActivity.getAccount() != null || MainActivity.getModerator() != null) {
                Intent intent = new Intent(MainActivity.this, AccountActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivityForResult(intent, 1);
            }
        } else if (id == R.id.nav_ratings) {
            if (account == null) {
                Toasty.info(getApplicationContext(), getString(R.string.login_first), Toast.LENGTH_LONG, true).show();
            } else {
                Intent intent = new Intent(MainActivity.this, UserRatingActivity.class);
                startActivity(intent);
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void setRatings(ArrayList<Rating> ratings) {
        MainActivity.ratings = ratings;
        notifyAdapter();
    }

    public static ArrayList<Rating> getRatings() {
        return ratings;
    }

    public static void updateRatings(Meal rated_meal) {
        MainActivity.ratings = rated_meal.getRatings();
        for(University university : universities) {
            ArrayList<Restaurant> restaurant_list = university.getRestaurants();
            for(Restaurant restaurant : restaurant_list) {
                ArrayList<com.example.lettu.data.Menu> menu_list = restaurant.getMenus();
                for(com.example.lettu.data.Menu menu : menu_list) {
                    ArrayList<Meal> meal_list = menu.getMeals();
                    for(Meal meal : meal_list) {
                        if(meal.getId() == rated_meal.getId()) {
                            meal.setRatings(ratings);
                            return;
                        }
                    }
                    menu.setMeals(meal_list);
                }
                restaurant.setMenus(menu_list);
            }
            university.setRestaurants(restaurant_list);
        }
    }


    public static ArrayList<University> getUniversities() {
        return universities;
    }


    public static void setUniversities(ArrayList<University> universities) {
        MainActivity.universities = universities;
    }
}
