package com.example.lettu.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.lettu.R;
import com.example.lettu.data.Account;
import com.example.lettu.data.Meal;
import com.example.lettu.data.Rating;
import com.example.lettu.adapter.RatingListAdapter;
import com.example.lettu.database.DatabaseHelper;
import com.example.lettu.database.DatabaseManager;


import java.util.ArrayList;

public class RatingActivity extends AppCompatActivity {

    private static final String TAG = "RatingActivity";

    RecyclerView list;
    EditText review;
    RatingBar ratingbar;
    ArrayList<Rating> ratings;
    ArrayList<Account> accounts;
    Account account;
    Rating current_rating;
    Meal meal;
    int rating_position;
    RatingListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        Log.d(TAG, "onCreate: started.");

        TextView meal_name      = findViewById(R.id.mealTextView);
        TextView category_name  = findViewById(R.id.categoryTextView);
        Button submit           = findViewById(R.id.submit);

        list                    = findViewById(R.id.reviewList);
        review                  = findViewById(R.id.reviewEditText);
        ratingbar               = findViewById(R.id.ratingBar);


        Bundle extras = getIntent().getExtras();
        meal = (Meal) getIntent().getSerializableExtra("meal");
        ratings = DatabaseManager.getInstance().getRatingsFromDatabaseWithValue(Integer.toString(meal.getId()), DatabaseHelper.COLUMN_RATING_MEAL_KEY);
        Log.d(TAG, "RATINGS" + ratings);

        accounts = DatabaseManager.getInstance().getAccountsFromDatabase();

        account = MainActivity.getAccount();
        // If account exists, get reviews from it
        if(account != null) {
            findUsersRating();
        } else {
            submit.setFocusable(false);
            ratingbar.isIndicator();
            review.setFocusable(false);
            review.setHint(getString(R.string.login_first));
        }
        meal_name.setText(meal.getName());
        category_name.setText(meal.getCategory());

        updateRatings();
    }


    public void submitRating(View v) {
        if (account == null) return;
        int stars = (int) ratingbar.getRating();
        String text = review.getText().toString();
        Integer ratingid = null;
        if(current_rating != null) {
            Log.d(TAG, "CURRENT RATING IS NOT NULL");
            ratingid = current_rating.getId();
            ratings.get(rating_position).setReview(text);
            ratings.get(rating_position).setStars(stars);
        }
        DatabaseManager.getInstance().setRating(ratingid, stars, text, Integer.toString(account.getId()), Integer.toString(meal.getId()));

        if (current_rating == null) {
            Log.d(TAG, "CURRENT RATING IS NULL");
            ratings = DatabaseManager.getInstance().getRatingsFromDatabaseWithValue(Integer.toString(meal.getId()), DatabaseHelper.COLUMN_RATING_MEAL_KEY);
            findUsersRating();
            updateRatings();
        }
        meal.setRatings(ratings);
        Log.d(TAG, "RATINGS" + ratings);
        adapter.notifyDataSetChanged();
    }


    public void updateRatings() {
        adapter = new RatingListAdapter(getApplicationContext(), ratings, accounts);
        list.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(RatingActivity.this);
        list.setLayoutManager(layoutManager);
        //list.setHasFixedSize(true);
    }

    private void findUsersRating() {
        int position = 0;
        for (Rating rating : ratings) {
            Log.d(TAG, rating.getId() + " === " + account.getUsername());
            if (rating.getAccountid() == account.getId()){
                current_rating = rating;
                rating_position = position;
                Log.d(TAG, "Arvostelu: " + rating.getReview() + rating.getStars());
                review.setText(rating.getReview());
                ratingbar.setRating(rating.getStars());
            }
            position++;
        }
    }

    @Override
    public void onBackPressed() {
        meal.setRatings(ratings);
        MainActivity.updateRatings(meal);
        Intent intent = new Intent(RatingActivity.this, MainActivity.class);
        startActivity(intent);
    }
}


