
package com.example.lettu.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.lettu.R;
import com.example.lettu.adapter.RatingListAdapter;
import com.example.lettu.adapter.UserRatingListAdapter;
import com.example.lettu.data.Account;
import com.example.lettu.data.Meal;
import com.example.lettu.data.Rating;
import com.example.lettu.database.DatabaseHelper;
import com.example.lettu.database.DatabaseManager;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class UserRatingActivity extends AppCompatActivity {

    private static final String TAG = "UserRatingActivity";

    Account account;
    UserRatingListAdapter adapter;
    RecyclerView user_rating_list;
    ArrayList<Meal> meals;
    Rating editable_rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_rating);
        Log.d(TAG, "onCreate: started.");
        user_rating_list = findViewById(R.id.user_review_list);

        account = MainActivity.getAccount();
        ArrayList<Rating> ratings = DatabaseManager.getInstance().getRatingsFromDatabaseWithValue(Integer.toString(account.getId()), DatabaseHelper.COLUMN_RATING_ACCOUNT_KEY);
        account.setRatings(ratings);

        meals = DatabaseManager.getInstance().getMealsFromDatabase();
        adapter = new UserRatingListAdapter(ratings, meals, this);
        user_rating_list.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(UserRatingActivity.this);
        user_rating_list.setLayoutManager(layoutManager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
