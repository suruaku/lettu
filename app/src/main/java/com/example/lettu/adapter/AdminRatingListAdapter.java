package com.example.lettu.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lettu.R;
import com.example.lettu.activity.RatingActivity;
import com.example.lettu.data.Meal;
import com.example.lettu.data.Rating;
import com.example.lettu.database.DatabaseHelper;
import com.example.lettu.database.DatabaseManager;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class AdminRatingListAdapter extends RecyclerView.Adapter<AdminRatingListAdapter.AdminRatingViewHolder> {
    private ArrayList<Rating> ratings;
    private ArrayList<Meal> meals;
    private Context context;

    // For debugging
    private static final String TAG = "AdminRatingListAdapter";

    public AdminRatingListAdapter(ArrayList<Rating> ratings, ArrayList<Meal> meals, Context context) {
        this.ratings    = ratings;
        this.context    = context;
        this.meals      = meals;
    }

    @Override
    public AdminRatingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_rating_list_item, parent, false);
        AdminRatingListAdapter.AdminRatingViewHolder holder = new AdminRatingListAdapter.AdminRatingViewHolder(view);
        return holder;
    }

    private Meal findMealWithId(int id) {
        for(Meal meal : meals) {
            if(meal.getId() == id)
                return meal;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(AdminRatingListAdapter.AdminRatingViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Rating rating = ratings.get(position);
        final Meal meal = findMealWithId(rating.getMealid());
        holder.user_stars.setRating(rating.getStars());
        holder.user_review.setText(rating.getReview());
        holder.user_meal.setText(meal.getName());

        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseManager.getInstance().removeRating(rating.getId());
                ratings.remove(rating);
                Toasty.info(context, "Rating removed!", Toast.LENGTH_LONG, true).show();
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return ratings.size();
    }

    public class AdminRatingViewHolder extends RecyclerView.ViewHolder{

        TextView user_review, user_meal;
        ConstraintLayout parent_layout;
        RatingBar user_stars;


        public AdminRatingViewHolder(View itemView) {
            super(itemView);
            parent_layout   = itemView.findViewById(R.id.user_layout);
            user_review     = itemView.findViewById(R.id.user_review);
            user_meal       = itemView.findViewById(R.id.user_meal);
            user_stars      = itemView.findViewById(R.id.user_stars);
        }
    }
}