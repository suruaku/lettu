package com.example.lettu.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lettu.R;
import com.example.lettu.activity.MainActivity;
import com.example.lettu.activity.RatingActivity;
import com.example.lettu.data.Meal;

import java.util.ArrayList;

public class MealListAdapter extends RecyclerView.Adapter<MealListAdapter.ViewHolder> {


    // For debugging
    private static final String TAG = "MealListAdapter";

    private ArrayList<Meal> meals;
    private Context context;
    private int position;


    public MealListAdapter(ArrayList<Meal> meals, Context context) {
        this.meals      = meals;
        this.context    = context;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public MealListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.meal_list_item, parent, false);
        MealListAdapter.ViewHolder holder = new MealListAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MealListAdapter.ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Meal meal = meals.get(position);
        holder.text_category.setText(meal.getCategory());
        holder.text_name.setText(meal.getName());
        holder.ratingbar.setRating(meal.getAverageRating());

        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on meal" + meals.get(position).getName());
                Intent intent = new Intent(context, RatingActivity.class);
                Meal meal = meals.get(position);
                intent.putExtra("meal", meal);
                context.startActivity(intent);
            }

        });
    }

    @Override
    public int getItemCount() {
        return meals.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        TextView text_category, text_name;
        ConstraintLayout parent_layout;
        RatingBar ratingbar;


        public ViewHolder(View itemView) {
            super(itemView);
            text_category   = itemView.findViewById(R.id.text_category);
            text_name       = itemView.findViewById(R.id.text_name);
            parent_layout   = itemView.findViewById(R.id.parent_layout);
            ratingbar       = itemView.findViewById(R.id.average_ratingbar);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            //if(MainActivity.getModerator() == null) return;
            contextMenu.setHeaderTitle("Admin actions");
            contextMenu.add(0, view.getId(), 1, "Delete meal");//groupId, itemId, order, title
            contextMenu.add(0, view.getId(), 2, "Edit meal");
            contextMenu.add(0, view.getId(), 3, "Add meal");
        }

    }
}