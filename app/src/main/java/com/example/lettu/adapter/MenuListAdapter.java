package com.example.lettu.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lettu.activity.MainActivity;
import com.example.lettu.data.Meal;
import com.example.lettu.R;
import com.example.lettu.activity.RatingActivity;
import com.example.lettu.data.Menu;
import com.example.lettu.data.Restaurant;

import java.util.ArrayList;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.ViewHolder> {

    // For debugging
    private static final String TAG = "MenuListAdapter";

    private ArrayList<Restaurant> restaurants;
    private Context context;

    public MenuListAdapter(ArrayList<Restaurant> restaurants, Context context) {
        this.restaurants    = restaurants;
        this.context        = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view           = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list_item, parent, false);
        ViewHolder holder   = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        holder.text_restaurant.setText(restaurants.get(position).getName());

        try {
            ArrayList<Menu> menus = new ArrayList<>();
            for (Menu menu : restaurants.get(position).getMenus()) {
                if (menu.getDate().equals(MainActivity.getISODate()))
                    menus.add(menu);
            }
            ArrayList<Meal> meals = menus.get(0).getMeals();
            holder.adapter = new MealListAdapter(meals, context);
            holder.list_meal.setAdapter(holder.adapter);
            holder.list_meal.setLayoutManager(new LinearLayoutManager(context));
        } catch (IndexOutOfBoundsException e){

        }

        /*
        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on meal" + meals.get(position).getName());
                Intent intent = new Intent(context, RatingActivity.class);
                intent.putExtra("mealid", meals.get(position).getId());
                context.startActivity(intent);
            }
        });
        */
    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView text_restaurant;
        RecyclerView list_meal;
        MealListAdapter adapter;

        public ViewHolder(View itemView) {
            super(itemView);
            text_restaurant     = itemView.findViewById(R.id.text_restaurant);
            list_meal           = itemView.findViewById(R.id.list_meal);
        }
    }
}
