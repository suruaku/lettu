package com.example.lettu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.lettu.R;
import com.example.lettu.data.Account;
import com.example.lettu.data.Rating;


import java.util.ArrayList;

public class RatingListAdapter extends RecyclerView.Adapter<RatingListAdapter.RatingViewHolder> {
    private ArrayList<Rating> ratings;
    private ArrayList<Account> accounts;
    private Context context;

    public RatingListAdapter(Context context, ArrayList<Rating> ratings, ArrayList<Account> accounts) {
        this.context    = context;
        this.ratings    = ratings;
        this.accounts   = accounts;
    }

    @Override
    public RatingListAdapter.RatingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rating_list_item, parent, false);
        RatingViewHolder viewHolder = new RatingViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RatingListAdapter.RatingViewHolder holder, int position) {
        holder.bindRating(ratings.get(position));
    }

    @Override
    public int getItemCount() {
        return ratings.size();
    }

    private Account findAccountWithId(int id) {
        for(Account account : accounts) {
            if(account.getId() == id)
                return account;
        }
        return null;
    }

    public class RatingViewHolder extends RecyclerView.ViewHolder {
        TextView review;
        TextView username;
        RatingBar ratingbar;

        private Context context;

        public RatingViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
        }

        public void bindRating(Rating rating) {
            username    = itemView.findViewById(R.id.username);
            review      = itemView.findViewById(R.id.nameTxt);
            ratingbar   = itemView.findViewById(R.id.ratingStars);

            Account account = findAccountWithId(rating.getAccountid());
            String username_from_id = account.getUsername();
            username.setText(username_from_id);
            review.setText(rating.getReview());
            ratingbar.setNumStars(rating.getStars());
            ratingbar.setRating((float)rating.getStars());
        }
    }
}