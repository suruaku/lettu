package com.example.lettu.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lettu.R;
import com.example.lettu.activity.RatingActivity;
import com.example.lettu.activity.UserRatingActivity;
import com.example.lettu.data.Account;
import com.example.lettu.data.Meal;
import com.example.lettu.data.Rating;
import com.example.lettu.database.DatabaseHelper;
import com.example.lettu.database.DatabaseManager;


import java.util.ArrayList;

public class UserRatingListAdapter extends RecyclerView.Adapter<UserRatingListAdapter.UserRatingViewHolder> {
    private ArrayList<Rating> ratings;
    private ArrayList<Meal> meals;
    private Context context;

    // For debugging
    private static final String TAG = "UserRatingListAdapter";

    public UserRatingListAdapter(ArrayList<Rating> ratings, ArrayList<Meal> meals, Context context) {
        this.ratings    = ratings;
        this.context    = context;
        this.meals      = meals;
    }

    @Override
    public UserRatingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_rating_list_item, parent, false);
        UserRatingListAdapter.UserRatingViewHolder holder = new UserRatingListAdapter.UserRatingViewHolder(view);
        return holder;
    }

    private Meal findMealWithId(int id) {
        for(Meal meal : meals) {
            if(meal.getId() == id)
                return meal;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(UserRatingListAdapter.UserRatingViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Rating rating = ratings.get(position);
        final Meal meal = findMealWithId(rating.getMealid());
        holder.user_stars.setRating(rating.getStars());
        holder.user_review.setText(rating.getReview());
        holder.user_meal.setText(meal.getName());

        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RatingActivity.class);
                meal.setRatings(DatabaseManager.getInstance().getRatingsFromDatabaseWithValue(Integer.toString(meal.getId()), DatabaseHelper.COLUMN_MEAL_PRIMARY_KEY));
                intent.putExtra("meal", meal);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return ratings.size();
    }

    public class UserRatingViewHolder extends RecyclerView.ViewHolder{

        TextView user_review, user_meal;
        ConstraintLayout parent_layout;
        RatingBar user_stars;


        public UserRatingViewHolder(View itemView) {
            super(itemView);
            parent_layout   = itemView.findViewById(R.id.user_layout);
            user_review     = itemView.findViewById(R.id.user_review);
            user_meal       = itemView.findViewById(R.id.user_meal);
            user_stars      = itemView.findViewById(R.id.user_stars);
        }
    }
}