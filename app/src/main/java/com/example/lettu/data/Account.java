package com.example.lettu.data;

import java.io.Serializable;
import java.util.ArrayList;

public class Account implements Serializable {
    private int id;
    private String username;
    private String password;
    private byte[] salt;
    private ArrayList<Rating> ratings;


    public Account(int id, String username, String password, byte[] salt){
        this.id   = id;
        this.username = username;
        this.password = password;
        this.salt     = salt;
        ratings       = new ArrayList<>();
    }

    @Override
    public String toString() {
        return (username + " id:" + id);
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRatings(ArrayList<Rating> ratings) {
        this.ratings = ratings;
    }

    // Add a new rating to user's own ratings
    public void addRating(Rating rating){
        ratings.add(rating);
    }

    // Remove a rating from user's own ratings
    public void removeRating(Rating removable_rating){
        for(Rating rating : ratings) {
            if(rating == removable_rating) {
                ratings.remove(rating);
                return;
            }
        }
    }

    // Edit a rating from user's own ratings
    public void editRating(Rating removable_rating){
        for(Rating rating : ratings) {
            if(rating == removable_rating) {
                ratings.remove(rating);
                return;
            }
        }
    }
}
