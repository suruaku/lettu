package com.example.lettu.data;

import java.io.Serializable;
import java.util.ArrayList;

public class Meal implements Serializable {
    private String name;
    private String category;
    private int id;
    private float average_rating;
    private ArrayList<Integer> menuids;
    private ArrayList<Rating> ratings;


    public Meal(String name, String category, int id){
        this.name = name;
        this.category = category;
        this.id = id;
        this.ratings = new ArrayList<>();
    }

    public void addMenuid(int menuid){
        this.menuids.add(menuid);
    }

    public ArrayList<Integer> getMenuids() {
        return menuids;
    }

    public void setRatings(ArrayList<Rating> ratings) {
        this.ratings = ratings;
    }

    // Give a rating for the meal
    public void giveRating(Rating rating) {
        ratings.add(rating);
    }

    // Calculate average rating for the meal
    public void calculateAverageRating() {
        int rating_sum = 0;
        for(Rating rating : ratings) {
            rating_sum += rating.getStars();
        }
        average_rating = (float)rating_sum/ratings.size();
    }

    @Override
    public String toString() {
        return(name+" "+category+" id:"+id);
    }

    public int getId(){ return this.id; }

    public String getCategory(){ return this.category; }

    public String getName(){
        return this.name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Rating> getRatings() {
        return ratings;
    }

    public float getAverageRating() {
        return average_rating;
    }
}

