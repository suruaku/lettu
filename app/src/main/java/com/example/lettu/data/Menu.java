package com.example.lettu.data;

import java.util.ArrayList;

public class Menu {
    private int id;
    private String date;
    private int restaurantid;
    private ArrayList<Meal> meals;


    public Menu(String date, int id, int restaurantid){
        this.date = date;
        this.id = id;
        this.restaurantid = restaurantid;
        this.meals = new ArrayList<>();
    }


    @Override
    public String toString() {
        return (date+" id:"+id);
    }

    public void setMeals(ArrayList<Meal> meals) {
        this.meals = meals;
    }

    // Add a meal to the list
    public void addMeal(Meal meal) {
        meals.add(meal);
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public ArrayList<Meal> getMeals() {
        return meals;
    }
}