package com.example.lettu.data;


public class Moderator extends Account {
    private int id;
    private String username;
    private String password;
    private byte[] salt;


    public Moderator(int id, String username, String password, byte[] salt){
        super(id, username, password, salt);
    }
}
