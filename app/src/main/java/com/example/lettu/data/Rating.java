package com.example.lettu.data;


import java.io.Serializable;

public class Rating implements Serializable {
    private int stars;
    private String review;
    private int id;
    private int accountid;
    private int mealid;


    public Rating(int stars, String review, int id, int accountid, int mealid) {
        this.stars = stars;
        this.review = review;
        this.id = id;
        this.accountid = accountid;
        this.mealid = mealid;
    }



    @Override
    public String toString() {
        return (stars+"/5 "+review);
    }

    public int getStars(){
        return stars;
    }

    public int getAccountid() {
        return accountid;
    }

    public int getMealid() {
        return mealid;
    }

    public String getReview() {
        return review;
    }

    public int getId() {
        return id;
    }

    public void setStars(int stars){
        this.stars = stars;
    }

    public void setReview(String review) {
        this.review = review;
    }

}