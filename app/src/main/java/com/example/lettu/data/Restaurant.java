package com.example.lettu.data;

import java.util.ArrayList;

public class Restaurant implements Cloneable{
    private String name;
    private int id;
    private ArrayList<Menu> menus;


    public Restaurant(String name, int id){
        this.name = name;
        this.id = id;
        this.menus = new ArrayList<>();
    }

    @Override
    public String toString() {
        return (name+" id:"+id);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMenus(ArrayList<Menu> menus) {
        this.menus = menus;
    }

    // Add a meal to the list
    public void addMenu(Menu menu) {
        menus.add(menu);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return (name);
    }

    public ArrayList<Menu> getMenus() {
        return menus;
    }
}