package com.example.lettu.data;

import java.util.ArrayList;

public class University {
    private String name;
    private ArrayList<Restaurant> restaurants;
    private ArrayList<Moderator> moderators;


    public University(String name){
        this.name = name;
        this.restaurants = new ArrayList<>();
        this.moderators = new ArrayList<>();
    }


    @Override
    public String toString() {
        return (name);
    }

    public void setRestaurants(ArrayList<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    public void setModerators(ArrayList<Moderator> moderators) {
        this.moderators = moderators;
    }

    // Add a restaurant to the list
    public void addRestaurant(Restaurant restaurant) {
        restaurants.add(restaurant);
    }

    public String getName() {
        return name;
    }

    public ArrayList<Restaurant> getRestaurants() {
        return restaurants;
    }
}
