package com.example.lettu.data;

import android.util.JsonWriter;
import android.util.Log;

import com.example.lettu.database.DatabaseManager;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class writeJSON {

	private static final String TAG = "WriteJSON";

	DatabaseManager databasemanager;

	private ArrayList<University> universities;
	private ArrayList<Restaurant> restaurants;
	private ArrayList<Meal> meals;
	private ArrayList<Rating> ratings;

	//Init and act as "main loop"
	public void writeJsonStream(OutputStream out) throws IOException {
		databasemanager   = DatabaseManager.getInstance();

		universities      = databasemanager.getUniversitiesFromDatabase();
		restaurants       = databasemanager.getRestaurantsFromDatabase();
		meals             = databasemanager.getMealsFromDatabase();
		ratings           = databasemanager.getRatingsFromDatabase();


		JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
		writer.setIndent("  ");
		writeJSONArray(writer, universities);
		writer.close();
	}


	// start JSON
	public void writeJSONArray(JsonWriter writer, ArrayList<University> universities) throws IOException {
		writer.beginArray();
		for(University university : universities) {
			writeUniversity(writer, university);
		}
		writer.endArray();
	}

	// write university's name
	public void writeUniversity(JsonWriter writer, University university) throws IOException {
		writer.beginObject();
		writer.name("name").value(university.toString());
		for(Restaurant restaurant : restaurants){
			writer.name("restaurant");
			writerRestautant(writer, restaurant);
		}
		writer.endObject();
	}

	// write restaurant's name
	public void writerRestautant(JsonWriter writer, Restaurant restaurant) throws IOException {
		writer.beginObject();
		writer.name("name").value(restaurant.getName());
		for(Meal meal : meals){
			writer.name("meal");
			writerMeal(writer, meal);
		}
		writer.endObject();
	}

	// write meal's name
	public void writerMeal(JsonWriter writer, Meal meal) throws IOException {
		writer.beginObject();
		writer.name("name").value(meal.getName());
		writer.name("category").value(meal.getCategory());
		for (Rating rating : ratings) {
			if (meal.getId() == rating.getMealid()) {
				writer.name("review");
				writerRating(writer, rating);
			}
		}
		writer.endObject();
	}

	// Write review
	public void writerRating(JsonWriter writer, Rating rating) throws IOException {
		writer.beginObject();
		writer.name("id").value(rating.getId());
		writer.name("userid").value(rating.getAccountid());
		writer.name("review").value(rating.getReview());
		writer.name("stars").value(rating.getStars());
		Log.d(TAG, "Review written.");
		writer.endObject();
	}
}