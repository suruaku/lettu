package com.example.lettu.database;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {

    private final Context context;

    private static String DATABASE_NAME                     = "lettu.db";
    private static String SQL_FILE_NAME                     = "database.sql";

    public static final String TABLE_UNIVERSITY                   = "university";
    public static final String TABLE_MENU                         = "menu";
    public static final String TABLE_RATING                       = "rating";
    public static final String TABLE_RESTAURANT                   = "restaurant";
    public static final String TABLE_MEAL                         = "meal";
    public static final String TABLE_ACCOUNT                      = "account";
    public static final String TABLE_MODERATOR                    = "moderator";
    public static final String TABLE_MEALS_MENUS                  = "meals_menus";

    public static final String COLUMN_ACCOUNT_PRIMARY_KEY         = "accountid";
    public static final String COLUMN_ACCOUNT_USERNAME            = "username";
    public static final String COLUMN_ACCOUNT_PASSWORD            = "password";
    public static final String COLUMN_ACCOUNT_SALT                = "salt";

    public static final String COLUMN_MODERATOR_PRIMARY_KEY       = "moderatorid";
    public static final String COLUMN_MODERATOR_USERNAME          = "username";
    public static final String COLUMN_MODERATOR_PASSWORD          = "password";

    public static final String COLUMN_UNIVERSITY_PRIMARY_KEY      = "name";

    public static final String COLUMN_RESTAURANT_PRIMARY_KEY      = "restaurantid";
    public static final String COLUMN_RESTAURANT_NAME             = "name";
    public static final String COLUMN_RESTAURANT_UNIVERSITY_KEY   = "universityid";

    public static final String COLUMN_MENU_PRIMARY_KEY            = "menuid";
    public static final String COLUMN_MENU_DATE                   = "dateday";
    public static final String COLUMN_MENU_RESTAURANT_KEY         = COLUMN_RESTAURANT_PRIMARY_KEY;

    public static final String COLUMN_MEAL_PRIMARY_KEY            = "mealid";
    public static final String COLUMN_MEAL_NAME                   = "name";
    public static final String COLUMN_MEAL_CATEGORY               = "category";
    public static final String COLUMN_MEAL_CYCLE                  = "cycle";
    public static final String COLUMN_MEAL_MENU_KEY               = COLUMN_MENU_PRIMARY_KEY;

    public static final String COLUMN_MEALS_MENUS_MENU_KEY        = COLUMN_MENU_PRIMARY_KEY;
    public static final String COLUMN_MEALS_MENUS_MEAL_KEY        = COLUMN_MEAL_PRIMARY_KEY;

    public static final String COLUMN_RATING_PRIMARY_KEY          = "ratingid";
    public static final String COLUMN_RATING_STARS                = "stars";
    public static final String COLUMN_RATING_REVIEW               = "review";
    public static final String COLUMN_RATING_ACCOUNT_KEY          = COLUMN_ACCOUNT_PRIMARY_KEY;
    public static final String COLUMN_RATING_MEAL_KEY             = COLUMN_MEAL_PRIMARY_KEY;

    private static int DATABASE_VERSION = 83;

    // Constructor for DatabaseHelper
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        String query = "PRAGMA foreign_keys = ON";
        db.execSQL(query);
        super.onConfigure(db);
    }

    @Override
    // Create new database file
    public void onCreate(SQLiteDatabase db) {
        // Read SQL queries from a file
        try {
            InputStream inputstream = context.getAssets().open(SQL_FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
            String line = reader.readLine();
            StringBuilder stringbuilder = new StringBuilder();
            while(line != null){
                stringbuilder.append(line).append("\n");
                if(line.contains(";")) {
                    String SQL_query = stringbuilder.toString();
                    db.execSQL(SQL_query);
                    stringbuilder.setLength(0);
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    // Remake the database, if current version version is newer
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_MEALS_MENUS);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_MODERATOR);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_RATING);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_ACCOUNT);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_MEAL);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_MENU);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_RESTAURANT);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_UNIVERSITY);
            onCreate(db);
        }
    }
}