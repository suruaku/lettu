package com.example.lettu.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.lettu.activity.MainActivity;
import com.example.lettu.data.Account;
import com.example.lettu.data.Meal;
import com.example.lettu.data.Menu;
import com.example.lettu.data.Moderator;
import com.example.lettu.data.Rating;
import com.example.lettu.data.Restaurant;
import com.example.lettu.data.University;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.example.lettu.database.DatabaseHelper.COLUMN_ACCOUNT_PASSWORD;
import static com.example.lettu.database.DatabaseHelper.COLUMN_ACCOUNT_SALT;
import static com.example.lettu.database.DatabaseHelper.COLUMN_ACCOUNT_USERNAME;
import static com.example.lettu.database.DatabaseHelper.COLUMN_ACCOUNT_PRIMARY_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MEAL_CATEGORY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MEALS_MENUS_MENU_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MEALS_MENUS_MEAL_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MEAL_NAME;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MEAL_PRIMARY_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MEAL_CYCLE;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MENU_DATE;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MENU_PRIMARY_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MENU_RESTAURANT_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MODERATOR_PASSWORD;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MODERATOR_PRIMARY_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_MODERATOR_USERNAME;
import static com.example.lettu.database.DatabaseHelper.COLUMN_RATING_ACCOUNT_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_RATING_MEAL_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_RATING_PRIMARY_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_RATING_REVIEW;
import static com.example.lettu.database.DatabaseHelper.COLUMN_RATING_STARS;
import static com.example.lettu.database.DatabaseHelper.COLUMN_RESTAURANT_NAME;
import static com.example.lettu.database.DatabaseHelper.COLUMN_RESTAURANT_PRIMARY_KEY;
import static com.example.lettu.database.DatabaseHelper.COLUMN_RESTAURANT_UNIVERSITY_KEY;
import static com.example.lettu.database.DatabaseHelper.TABLE_ACCOUNT;
import static com.example.lettu.database.DatabaseHelper.TABLE_MEAL;
import static com.example.lettu.database.DatabaseHelper.TABLE_MEALS_MENUS;
import static com.example.lettu.database.DatabaseHelper.TABLE_MENU;
import static com.example.lettu.database.DatabaseHelper.TABLE_MODERATOR;
import static com.example.lettu.database.DatabaseHelper.TABLE_RATING;
import static com.example.lettu.database.DatabaseHelper.TABLE_RESTAURANT;
import static com.example.lettu.database.DatabaseHelper.TABLE_UNIVERSITY;

public class DatabaseManager {

    private static final String TAG = "DatabaseManager";

    private AtomicInteger opencounter = new AtomicInteger();

    private static DatabaseManager instance;
    private static SQLiteOpenHelper databasehelper;
    private SQLiteDatabase database;

    public static synchronized void initializeInstance(SQLiteOpenHelper helper) {
        if (instance == null) {
            instance = new DatabaseManager();
            databasehelper = helper;
        }
    }

    public static synchronized DatabaseManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException(DatabaseManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }

        return instance;
    }

    public synchronized SQLiteDatabase openDatabase() {
        if(opencounter.incrementAndGet() == 1) {
            // Opening new database
            database = databasehelper.getWritableDatabase();
            String query = "PRAGMA foreign_keys = ON";
            database.execSQL(query);
        }
        return database;
    }

    public synchronized void closeDatabase() {
        if(opencounter.decrementAndGet() == 0) {
            // Closing database
            database.close();
        }
    }

    /**************************************
     Methods for removing rows from database
     **************************************/

    // Remove existing meal from the database
    public boolean removeMeal(Integer id){
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        return db.delete(TABLE_MEAL, COLUMN_MEAL_PRIMARY_KEY + "=?", new String[]{Integer.toString(id)}) > 0;
    }

    // Remove existing restaurant from the database
    public boolean removeRestaurant(Integer id){
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        return db.delete(TABLE_RESTAURANT, COLUMN_RESTAURANT_PRIMARY_KEY + "=?", new String[]{Integer.toString(id)}) > 0;
    }

    // Remove existing account from the database
    public boolean removeAccount(Integer id){
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        return db.delete(TABLE_ACCOUNT, COLUMN_ACCOUNT_PRIMARY_KEY + "=?", new String[]{Integer.toString(id)}) > 0;
    }

    // Remove existing rating from the database
    public boolean removeRating(Integer id){
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        return db.delete(TABLE_RATING, COLUMN_RATING_PRIMARY_KEY + "=?", new String[]{Integer.toString(id)}) > 0;
    }

    // Remove existing menu from the database
    public boolean removeMenu(Integer id){
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        return db.delete(TABLE_MENU, COLUMN_MENU_PRIMARY_KEY + "=?", new String[]{Integer.toString(id)}) > 0;
    }

    /***************************************************************
     Methods for editing existing rows in database or adding new rows
     ***************************************************************/

    // Edits already existing account or adds a new one
    public void setAccount(Integer id, String username, String hashed_password, byte[] salt) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ACCOUNT_PASSWORD, hashed_password);
        contentValues.put(COLUMN_ACCOUNT_SALT, salt);
        contentValues.put(COLUMN_ACCOUNT_USERNAME, username);

        if(id != null) {
            // Edit existing account
            db.update(TABLE_ACCOUNT, contentValues, COLUMN_ACCOUNT_PRIMARY_KEY+"=?", new String[]{Integer.toString(id)});
        } else {
            // Add a new account
            db.insert(TABLE_ACCOUNT, null, contentValues);
        }
        //DatabaseManager.getInstance().closeDatabase();
    }

    // Edit already existing meal or adds a new one if mealid equals -1
    public Integer setMeal(Integer mealid, Integer menuid, String name, String category) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_MEAL_PRIMARY_KEY, mealid);
        contentValues.put(COLUMN_MEAL_NAME, name);
        contentValues.put(COLUMN_MEAL_CATEGORY, category);
        if(mealid != null) {
            // Edit existing meal
            db.update(TABLE_MEAL, contentValues, COLUMN_MEAL_PRIMARY_KEY+"=?", new String[]{Integer.toString(mealid)});
        } else {
            // Add a new meal
            db.insert(TABLE_MEAL, null, contentValues);
            String query = "SELECT last_insert_rowid()";
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            mealid = Integer.parseInt(cursor.getString(0));
        }
        return mealid;
        //DatabaseManager.getInstance().closeDatabase();
    }

    // Edit already existing menu or adds a new one if menuid is null
    public Integer setMenu(Integer menuid, int restaurantid, String dateday) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_MENU_PRIMARY_KEY, menuid);
        contentValues.put(COLUMN_MENU_RESTAURANT_KEY, restaurantid);
        contentValues.put(COLUMN_MENU_DATE, dateday);
        if(menuid != null) {
            // Edit existing menu
            db.update(TABLE_MENU, contentValues, COLUMN_MENU_PRIMARY_KEY+"=?", new String[]{Integer.toString(menuid)});
        } else {
            // Add a new menu
            db.insert(TABLE_MENU, null, contentValues);
            String query = "SELECT last_insert_rowid()";
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            menuid = Integer.parseInt(cursor.getString(0));
        }
        return menuid;
        //DatabaseManager.getInstance().closeDatabase();
    }

    // Edit already existing restaurant or adds a new one if restaurantid equals -1
    public Integer setRestaurant(Integer restaurantid, String universityid, String name) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_RESTAURANT_PRIMARY_KEY, restaurantid);
        contentValues.put(COLUMN_RESTAURANT_UNIVERSITY_KEY, universityid);
        contentValues.put(COLUMN_RESTAURANT_NAME, name);
        if(restaurantid != null) {
            // Edit existing restaurant
            db.update(TABLE_RESTAURANT, contentValues, COLUMN_RESTAURANT_NAME+"=?", new String[]{Integer.toString(restaurantid)});
        } else {
            // Add a new restaurant
            db.insert(TABLE_RESTAURANT, null, contentValues);
            String query = "SELECT last_insert_rowid()";
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            restaurantid = Integer.parseInt(cursor.getString(0));
        }
        return restaurantid;
        //DatabaseManager.getInstance().closeDatabase();
    }

    // Edit already existing rating or adds a new one if ratingid equals -1
    public Integer setRating(Integer ratingid, int stars, String review, String username, String mealid) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_RATING_STARS, stars);
        contentValues.put(COLUMN_RATING_REVIEW, review);
        contentValues.put(COLUMN_RATING_ACCOUNT_KEY, username);
        contentValues.put(COLUMN_RATING_MEAL_KEY, mealid);
        //if(doesRowExist(TABLE_RATING, Integer.toString(ratingid), COLUMN_RATING_PRIMARY_KEY)) {
        if(ratingid != null){
            // Edit existing rating
            db.update(TABLE_RATING, contentValues, COLUMN_RATING_PRIMARY_KEY+"=?", new String[]{Integer.toString(ratingid)});
        } else {
            // Add a new rating
            db.insert(TABLE_RATING, null, contentValues);
            String query = "SELECT last_insert_rowid()";
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            ratingid = Integer.parseInt(cursor.getString(0));
        }
        return ratingid;
        //DatabaseManager.getInstance().closeDatabase();
    }

    /************************************************************************************************
     Getters for every class. Following methods return a list containing objects created from database.
     ************************************************************************************************/
    
    // Get a list of universities from database
    public ArrayList<University> getUniversitiesFromDatabase() {
        ArrayList<University> universityList = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_UNIVERSITY;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(0);
                University university = new University(name);
                universityList.add(university);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return universityList;
    }

    // Get a list of universities from database with value
    public ArrayList<University> getUniversitiesFromDatabaseWithValue(String value, String column) {
        ArrayList<University> universityList = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_UNIVERSITY+" WHERE "+column+"=?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{value});

        // Looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(0);
                University university = new University(name);
                universityList.add(university);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return universityList;
    }


    // Get a list of meals from database
    public ArrayList<Meal> getMealsFromDatabase() {
        ArrayList<Meal> mealList = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_MEAL;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                int mealid = Integer.parseInt(cursor.getString(0));
                String name = cursor.getString(1);
                String category = cursor.getString(2);
                Meal meal = new Meal(name, category, mealid);
                mealList.add(meal);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return mealList;
    }

    // Get a list of meals from database with value
    public ArrayList<Meal> getMealsFromDatabaseWithValue(String value, String column) {
        ArrayList<Meal> mealList = new ArrayList<>();
        String selectQuery = "SELECT "+TABLE_MEAL+"."+COLUMN_MEAL_PRIMARY_KEY+","+COLUMN_MEAL_NAME+","+COLUMN_MEAL_CATEGORY+","+COLUMN_MEALS_MENUS_MENU_KEY+" " +
                "FROM "+TABLE_MEAL+" " +
                "INNER JOIN "+TABLE_MEALS_MENUS+
                " ON "+TABLE_MEAL+"."+COLUMN_MEAL_PRIMARY_KEY+"="+TABLE_MEALS_MENUS+"."+COLUMN_MEALS_MENUS_MEAL_KEY+
                " WHERE "+column+"=?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{value});

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                int mealid = Integer.parseInt(cursor.getString(0));
                String name = cursor.getString(1);
                String category = cursor.getString(2);
                Meal meal = new Meal(name, category, mealid);
                mealList.add(meal);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return mealList;
    }

    // Get a list of accounts from database
    public ArrayList<Account> getAccountsFromDatabase() {
        ArrayList<Account> accounts = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_ACCOUNT;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                int    id       = cursor.getInt(0);
                String username = cursor.getString(1);
                String password = cursor.getString(2);
                byte[] salt     = cursor.getBlob(3);
                Account account = new Account(id, username, password, salt);
                accounts.add(account);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return accounts;
    }


    // Get a list of accounts from database with value
    public ArrayList<Account> getAccountsFromDatabaseWithValue(String value, String column) {
        ArrayList<Account> accounts = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_ACCOUNT+" WHERE "+column+"=?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{value});

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                int    id       = cursor.getInt(0);
                String username = cursor.getString(1);
                String password = cursor.getString(2);
                byte[] salt     = cursor.getBlob(3);
                Account account = new Account(id, username, password, salt);
                accounts.add(account);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return accounts;
    }

    // Get a list of ratings from database
    public ArrayList<Rating> getRatingsFromDatabase() {
        ArrayList<Rating> ratings = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_RATING;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                int ratingid = Integer.parseInt(cursor.getString(0));
                int stars = Integer.parseInt(cursor.getString(1));
                String review = cursor.getString(2);
                int accountid = Integer.parseInt(cursor.getString(3));
                int mealid = Integer.parseInt(cursor.getString(4));

                Rating rating = new Rating(stars, review, ratingid, accountid, mealid);
                ratings.add(rating);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return ratings;
    }


    // Get a list of ratings from database with value
    public ArrayList<Rating> getRatingsFromDatabaseWithValue(String value, String column) {
        ArrayList<Rating> ratings = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_RATING+" WHERE "+column+"=?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{value});

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                int ratingid = Integer.parseInt(cursor.getString(0));
                int stars = Integer.parseInt(cursor.getString(1));
                String review = cursor.getString(2);
                int accountid = Integer.parseInt(cursor.getString(3));
                int mealid = Integer.parseInt(cursor.getString(4));

                Rating rating = new Rating(stars, review, ratingid, accountid, mealid);
                ratings.add(rating);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return ratings;
    }


    // Get a list of restaurants from database
    public ArrayList<Restaurant> getRestaurantsFromDatabase() {
        ArrayList<Restaurant> restaurants = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_RESTAURANT;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(2);
                int restaurantid = Integer.parseInt(cursor.getString(0));

                Restaurant restaurant = new Restaurant(name, restaurantid);
                restaurants.add(restaurant);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return restaurants;
    }

    // Get a list of restaurants from database with value
    public ArrayList<Restaurant> getRestaurantsFromDatabaseWithValue(String value, String column) {
        ArrayList<Restaurant> restaurants = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_RESTAURANT+" WHERE "+column+"=?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{value});

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(2);
                int restaurantid = Integer.parseInt(cursor.getString(0));

                Restaurant restaurant = new Restaurant(name, restaurantid);
                restaurants.add(restaurant);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return restaurants;
    }

    // Get a list of menus from database
    public ArrayList<Menu> getMenusFromDatabase() {
        ArrayList<Menu> menus = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_MENU;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                String date = cursor.getString(2);
                int restaurantid = Integer.parseInt(cursor.getString(1));
                int menuid = Integer.parseInt(cursor.getString(0));

                Menu menu = new Menu(date, menuid, restaurantid);
                menus.add(menu);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return menus;
    }


    // Get a list of menus from database with value
    public ArrayList<Menu> getMenusFromDatabaseWithValue(String value, String column) {
        ArrayList<Menu> menus = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_MENU+" WHERE "+column+"=?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{value});

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                String date = cursor.getString(2);
                int restaurantid = Integer.parseInt(cursor.getString(1));
                int menuid = Integer.parseInt(cursor.getString(0));

                Menu menu = new Menu(date, menuid, restaurantid);
                menus.add(menu);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return menus;
    }


    // Get a list of moderators from database
    public ArrayList<Moderator> getModeratorsFromDatabase() {
        ArrayList<Moderator> moderators = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_MODERATOR;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                int    id       = cursor.getInt(0);
                String username = cursor.getString(1);
                String password = cursor.getString(2);
                byte[] salt     = cursor.getBlob(3);

                Moderator moderator = new Moderator(id, username, password, salt);
                moderators.add(moderator);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return moderators;
    }

    // Get a list of moderators from database with Value
    public ArrayList<Moderator> getModeratorsFromDatabaseWithValue(String value, String column) {
        ArrayList<Moderator> moderators = new ArrayList<>();
        String selectQuery = "SELECT * FROM "+TABLE_MODERATOR+" WHERE "+column+"=?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{value});

        // looping through all rows and adding to the list
        if (cursor.moveToFirst()) {
            do {
                int    id       = cursor.getInt(0);
                String username = cursor.getString(1);
                String password = cursor.getString(2);
                byte[] salt     = cursor.getBlob(3);


                Moderator moderator = new Moderator(id, username, password, salt);
                moderators.add(moderator);
            } while (cursor.moveToNext());
        }
        cursor.close();
        //DatabaseManager.getInstance().closeDatabase();
        return moderators;
    }

    // Return id, if moderator exists with given username
    public Integer getModeratorId(String username) {
        Log.d(TAG, "Getting moderator account");
        String query = "SELECT "+COLUMN_MODERATOR_PRIMARY_KEY+" " +
                "FROM "+TABLE_MODERATOR +
                " WHERE "+COLUMN_MODERATOR_USERNAME+" = ?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{username});

        if (cursor.moveToFirst()) {
            Log.d(TAG, "MODERATOR EXISTS");
            return Integer.parseInt(cursor.getString(0));
        }
        return null;
    }


    // Return id, if account  exists with given username
    public Integer getAccountId(String username) {
        Log.d(TAG, "Getting user account");
        String query = "SELECT "+COLUMN_ACCOUNT_PRIMARY_KEY+" " +
                "FROM "+TABLE_ACCOUNT +
                " WHERE "+COLUMN_ACCOUNT_USERNAME+" = ?";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{username});

        if (cursor.moveToFirst()) {
            Log.d(TAG, "USERNAME EXISTS");
            return Integer.parseInt(cursor.getString(0));
        }
        return null;
    }


}
